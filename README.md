# TopScorers

To start your Phoenix server:

- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.setup`
- Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Notes

### Initial setup

`mix phx.new top-scorers --no-html --no-assets --no-gettext --no-live --no-dashboard --no-mailer`
We don't need any of excluded features for the task, so don't generate them in the first place.

While the exercise states a port of 3000 I kept the standard of 4000. I guess that part was written by somebody coming from Rails and is not there as a trap for the careless reader :D

### No Phoenix context

I decided against using a context since the requirements are pretty small, I felt a context would be too much overhead.
Most of the business logic is inside `top_scorers/score/impl.ex`.

### Seeds

Generating a million Rows is kind of expensive. This is optimized by reusing the same entry for each row with the `placeholder` feature.
This makes the seeds run in ~5 seconds on my machine, which is good enough for now.

### Starting the GenServer (and Task) as part of the Application tree

The `ScoreServer` and `PeriodicTask` are started as part of the Application if it is not in a test environment.
In tests, the components are started only for tests that need them locally, reducing access of global state and side effects.

### Limited approach

The `TopScorers.Score` behaviour is used to encapsulate the domain logic so that it can be mocked in tests with `Mox`.
This is especially useful since that way we can get rid of the randomness part in tests.

### Using an additional Task for updating with `restart: :permanent`

While the requirements of the exercise states, that the update should be part of the GenServer itself, i decided that it's a better speration of concerns to wrap it into a thin abstraction `TopScorers.PeriodicTask`.
Due to this seperation a crash in the update functionality won't lead to a loss of the state in the GenServer and the code has a nicer organisation.
If the requirements didn't explicitly stated to use `GenServer` I might have started with an `Agent` to implement the `ScoreServer`.
Also it has to be noted that the update does not happen exactly every 60 seconds, due to the needed time to update the database, the state of `ScoreServer` and that `Process.slepp` isn't guaranteed to end after exactly the specified time.

### Update of 1 Million Rows

The update of all Users with new `points` every minute is pretty expensive.
On my machine it takes ~3-5 seconds to do so right now, which is kind of ok since I don't have more requirements and the updating is non blocking, neither on the Application (because the updating is run in it's own Task) nor on the databse.

If there where stricter requirements there are some options that could be taken to optimize:

- Database sharding
- Batching of updates

### Querying the Users without an index

There is no index on the `User.points` field, due to two reasons:

- Having an index could probably impact write performance, which is already the slowest part of the system (allthough in testing there wasn't really any impact with an index vs. without)
- For some reason having an index increased the query duration by an order of magnitude (~4 ms without query cache and index, 0.3 ms with query cache and without index vs ~48ms without query cache with index and ~7ms with query cache and with index). Probably worth investigating later.

### Next steps

Some things I could think of that should be added next:

- typespecs
- fixtures for tests
