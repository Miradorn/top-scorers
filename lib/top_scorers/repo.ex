defmodule TopScorers.Repo do
  use Ecto.Repo,
    otp_app: :top_scorers,
    adapter: Ecto.Adapters.Postgres
end
