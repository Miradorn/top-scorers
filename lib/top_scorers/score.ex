defmodule TopScorers.Score do
  @moduledoc """
  Behaviour module to specify logic to hande Scores
  """

  @doc """
  Return a new random number between 0 and 100 (inclusive).
  """
  @callback new_max_number() :: integer()

  @doc """
  Returns upto 2 TopScorers.User with `points` greater than `above`.
  """
  @callback users_with_points_above(number(), number()) :: list(TopScorers.User.t())
  @callback users_with_points_above(number()) :: list(TopScorers.User.t())

  @doc """
  Updates all `User`s with new randomized points between 0 and 100 (inclusive).

  Returns number of updated `User`s.
  """
  @callback randomize_user_points() :: integer()
end
