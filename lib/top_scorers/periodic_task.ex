defmodule TopScorers.PeriodicTask do
  @moduledoc """
  Helper task to run some function periodically.
  """
  use Task, restart: :permanent

  def start_link(args \\ []) do
    Task.start_link(__MODULE__, :run, args)
  end

  @doc """
  Sleeps for `period`, than executes the work
  """
  def run(period \\ 60 * 1_000, {module, func, args}) do
    Process.sleep(period)

    apply(module, func, args)
  end
end
