defmodule TopScorers.ScoreServer do
  @moduledoc """
  GenServer to handle the main logic of the challenge.
  """
  use GenServer

  defmodule State do
    @moduledoc """
    Holds the state of the ScoreManager
    """

    @enforce_keys [:max_number]
    defstruct @enforce_keys ++ [:last_accessed_at]
  end

  # Client Code

  @doc """
  Client function to start up an ScoreServer GenServer
  """
  def start_link(opts \\ []) do
    name = Keyword.get(opts, :name, __MODULE__)

    {:ok, pid} =
      GenServer.start_link(
        __MODULE__,
        %State{max_number: impl().new_max_number()},
        name: name
      )

    {:ok, pid}
  end

  @doc """
  Helper client function to get current state of GenServer.

  Returns the current state.
  """
  def state(name \\ __MODULE__) do
    GenServer.call(name, :state)
  end

  @doc """
  Client function to retriever Users with points > max_number.
  Updates `last_accessed_at` to current time.

  Returns a List of at most 2 Users.
  """
  def users_with_points_above_max(name \\ __MODULE__) do
    GenServer.call(name, :above_max)
  end

  @doc """
  Updates all users with new points and randomizes max_number.
  """
  def periodic_update(name \\ __MODULE__) do
    GenServer.cast(name, :periodic_update)
  end

  # Server Code
  @impl true
  def init(state) do
    {:ok, state}
  end

  @impl true
  def handle_call(:state, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_call(:above_max, _from, state) do
    max = state.max_number
    users = impl().users_with_points_above(max)

    {:reply, users, %{state | last_accessed_at: TopScorers.formatted_timestamp()}}
  end

  @impl true
  def handle_cast(:periodic_update, state) do
    impl().randomize_user_points()
    {:noreply, %{state | max_number: impl().new_max_number()}}
  end

  defp impl, do: Application.get_env(:top_scorers, :score, TopScorers.Score.Impl)
end
