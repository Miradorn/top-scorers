defmodule TopScorers.Score.Impl do
  @moduledoc """
  Implementation of `Score` logic.
  """
  @behaviour TopScorers.Score

  import Ecto.Query
  alias TopScorers.{Repo, User, Score}

  @impl Score
  def new_max_number do
    0..100 |> Enum.random()
  end

  @impl Score
  def users_with_points_above(above, limit \\ 2) do
    query = from u in User, where: u.points > ^above, limit: ^limit
    Repo.all(query)
  end

  @impl Score
  def randomize_user_points do
    query = from u in User, update: [set: [points: fragment("floor(random() * 101)")]]
    {count, _} = Repo.update_all(query, [])

    count
  end
end
