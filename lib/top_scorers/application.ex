defmodule TopScorers.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children =
      [
        # Start the Ecto repository
        TopScorers.Repo,
        # Start the Telemetry supervisor
        TopScorersWeb.Telemetry,
        # Start the PubSub system
        {Phoenix.PubSub, name: TopScorers.PubSub},
        # Start the Endpoint (http/https)
        TopScorersWeb.Endpoint
        # Start a worker by calling: TopScorers.Worker.start_link(arg)
        # {TopScorers.Worker, arg}
      ] ++ env_children(Mix.env())

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TopScorers.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def env_children(:test), do: []

  def env_children(_),
    do: [TopScorers.ScoreServer, {TopScorers.PeriodicTask, [{TopScorers.ScoreServer, :periodic_update, []}]}]

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    TopScorersWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
