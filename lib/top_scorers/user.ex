defmodule TopScorers.User do
  @moduledoc """
  Ecto Schema for the central User type
  """
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          points: integer(),
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "users" do
    field :points, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_required([:points])
    |> validate_number(:points, greater_than_or_equal_to: 0, less_than_or_equal_to: 100)
  end
end
