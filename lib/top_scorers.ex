defmodule TopScorers do
  @moduledoc """
  TopScorers keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @doc """
  Helper function to create a timestamp useable in Ecto timestamp fields.
  """
  def formatted_timestamp(from \\ NaiveDateTime.utc_now()) do
    from |> NaiveDateTime.truncate(:second)
  end
end
