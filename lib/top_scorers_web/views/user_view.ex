defmodule TopScorersWeb.UserView do
  use TopScorersWeb, :view

  def render("index.json", %{users: users, timestamp: nil}) do
    %{timestamp: nil, users: render_many(users, __MODULE__, "user.json")}
  end

  def render("index.json", %{users: users, timestamp: timestamp}) do
    %{
      timestamp: timestamp |> NaiveDateTime.to_iso8601(),
      users: render_many(users, __MODULE__, "user.json")
    }
  end

  def render("user.json", %{user: user}) do
    %{id: user.id, points: user.points}
  end
end
