defmodule TopScorersWeb.Router do
  use TopScorersWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TopScorersWeb do
    pipe_through :api

    get "/", UserController, :index
  end
end
