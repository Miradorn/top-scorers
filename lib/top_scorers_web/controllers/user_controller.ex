defmodule TopScorersWeb.UserController do
  use TopScorersWeb, :controller

  alias TopScorers.ScoreServer

  def index(conn, _params) do
    timestamp = ScoreServer.state().last_accessed_at
    users = ScoreServer.users_with_points_above_max()

    render(conn, "index.json", %{timestamp: timestamp, users: users})
  end
end
