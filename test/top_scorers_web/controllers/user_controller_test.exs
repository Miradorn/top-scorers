defmodule TopScorersWeb.UserControllerTest do
  # Can't be run async, since we need to mock out a global dependency.
  use TopScorersWeb.ConnCase, async: false

  import Mox

  alias TopScorers.{User, Repo}

  describe "index" do
    setup :set_mox_from_context
    setup :verify_on_exit!

    test "it returns users and the timestamp from the previous call", %{conn: conn} do
      timestamp = TopScorers.formatted_timestamp()

      users = [
        %{points: 49, inserted_at: timestamp, updated_at: timestamp},
        %{points: 50, inserted_at: timestamp, updated_at: timestamp},
        %{points: 51, inserted_at: timestamp, updated_at: timestamp},
        %{points: 52, inserted_at: timestamp, updated_at: timestamp},
        %{points: 53, inserted_at: timestamp, updated_at: timestamp}
      ]

      Repo.insert_all(User, users)

      TopScorers.Score.Mock
      |> stub_with(TopScorers.Score.Impl)
      |> expect(:new_max_number, fn -> 50 end)

      start_supervised(TopScorers.ScoreServer)

      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"timestamp" => nil, "users" => users} = json_response(conn, 200)

      assert length(users) == 2
      Enum.each(users, fn user -> assert user["points"] >= 50 end)

      conn = get(conn, Routes.user_path(conn, :index))
      assert %{"timestamp" => last_accessed_at_string, "users" => users} = json_response(conn, 200)

      last_accessed_at = NaiveDateTime.from_iso8601!(last_accessed_at_string)
      assert length(users) == 2
      Enum.each(users, fn user -> assert user["points"] >= 50 end)

      assert NaiveDateTime.diff(timestamp, last_accessed_at) <= 1
    end
  end
end
