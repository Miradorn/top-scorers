defmodule TopScorersWeb.UserViewTest do
  use TopScorersWeb.ConnCase, async: true

  import Phoenix.View

  alias TopScorersWeb.UserView

  describe "index.json" do
    test "renders without timestamp" do
      data = %{users: [%{id: 1, points: 5}], timestamp: nil}
      assert render(UserView, "index.json", data) == data
    end

    test "renders human readable timestamp" do
      users = [%{id: 1, points: 5}]
      timestamp = ~N[2022-04-17 10:18:21]
      readable = "2022-04-17T10:18:21"

      assert render(UserView, "index.json", %{users: users, timestamp: timestamp}) == %{
               users: users,
               timestamp: readable
             }
    end
  end

  describe "user.json" do
    test "only renders id and points" do
      user = %{id: 1, points: 20, extra: "shouldnt be rendered"}

      assert render(UserView, "user.json", %{user: user}) == %{id: user.id, points: user.points}
    end
  end
end
