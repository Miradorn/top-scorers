defmodule TopScorersTest do
  use ExUnit.Case

  describe "formatted_timestamp/1" do
    test "it cuts of after seconds" do
      original = ~N[2022-04-28T06:22:07.12345]

      formatted = TopScorers.formatted_timestamp(original)

      assert ~N[2022-04-28T06:22:07] == formatted
    end
  end
end
