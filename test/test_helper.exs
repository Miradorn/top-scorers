ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(TopScorers.Repo, :manual)

Mox.defmock(TopScorers.Score.Mock, for: TopScorers.Score)

Application.put_env(:top_scorers, :score, TopScorers.Score.Mock)
