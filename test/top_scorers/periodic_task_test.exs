defmodule TopScorers.PeriodicTaskTest do
  alias TopScorers.PeriodicTask

  use ExUnit.Case, async: true

  describe "start_link/2" do
    defmodule Callback do
      def call(pid) do
        send(pid, :called)
      end
    end

    # Okay, this test is not really great, but it does kind of test the behaviour.
    # In a more serious setting I would have gone using a well tested library like https://hexdocs.pm/quantum/readme.html
    # or https://github.com/sorentwo/oban .
    test "it calls given module repeatedly after period" do
      start_supervised({PeriodicTask, [100, {TopScorers.PeriodicTaskTest.Callback, :call, [self()]}]})

      refute_receive :called, 90

      # Time is brittle, so give our task ~40 ms extra time to send its message
      assert_receive :called, 50

      refute_receive :called, 90

      # Time is brittle, so give our task ~400 ms extra time to send its message
      assert_receive :called, 50
    end
  end
end
