defmodule TopScorers.UserTest do
  use TopScorers.DataCase, async: true
  alias TopScorers.User

  test "points must be between 0 and 100" do
    changeset = User.changeset(%User{}, %{points: 0})
    assert changeset.valid?

    changeset = User.changeset(%User{}, %{points: -1})
    refute changeset.valid?

    changeset = User.changeset(%User{}, %{points: 100})
    assert changeset.valid?

    changeset = User.changeset(%User{}, %{points: 101})
    refute changeset.valid?
  end
end
