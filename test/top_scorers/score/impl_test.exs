defmodule TopScorers.Score.ImplTest do
  use TopScorers.DataCase

  alias TopScorers
  alias TopScorers.{Repo, User}
  alias TopScorers.Score.Impl

  describe "users_with_points_above/2" do
    setup do
      max_number = 20
      timestamp = TopScorers.formatted_timestamp()

      testdata = [
        %{points: max_number - 1, inserted_at: timestamp, updated_at: timestamp},
        %{points: max_number, inserted_at: timestamp, updated_at: timestamp},
        %{points: max_number + 1, inserted_at: timestamp, updated_at: timestamp},
        %{points: max_number + 2, inserted_at: timestamp, updated_at: timestamp},
        %{points: max_number + 3, inserted_at: timestamp, updated_at: timestamp}
      ]

      Repo.insert_all(User, testdata)

      :ok
    end

    test "receives all users with more than max_number points" do
      users = Impl.users_with_points_above(20, nil)
      assert length(users) == 3

      Enum.each(users, fn user -> assert user.points > 20 end)

      users = Impl.users_with_points_above(21, nil)
      assert length(users) == 2

      Enum.each(users, fn user -> assert user.points > 20 end)
    end

    test "receives only `limit` users at most" do
      users = Impl.users_with_points_above(10, 2)
      assert length(users) == 2
    end
  end

  describe "randomize_user_points/0" do
    setup do
      # deliberately use a number that is not allowed, so that the update logic will override it.
      # Since `update_all` is used, validations will not run.
      points = -5
      timestamp = TopScorers.formatted_timestamp()

      testdata = [
        %{points: points, inserted_at: timestamp, updated_at: timestamp},
        %{points: points, inserted_at: timestamp, updated_at: timestamp},
        %{points: points, inserted_at: timestamp, updated_at: timestamp}
      ]

      Repo.insert_all(User, testdata)

      :ok
    end

    test "it updates all users points to a new number" do
      assert 3 == Impl.randomize_user_points()

      users = Repo.all(User)

      assert length(users) == 3

      Enum.each(users, fn user ->
        assert user.points <= 100
        assert user.points >= 0
      end)
    end
  end
end
