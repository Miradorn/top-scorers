defmodule TopScorers.ScoreServerTest do
  use TopScorers.DataCase, async: true

  alias TopScorers.ScoreServer

  import Mox

  setup :verify_on_exit!

  describe "start_link/1" do
    test "it initializes its state" do
      TopScorers.Score.Mock
      |> expect(:new_max_number, fn -> 50 end)

      # We can't use `start_supervised` here, since we can't allow the mock to be called from the supervisor pid 
      # (since it's unknown), therefore initialization has to happen in the test Process.
      {:ok, _} = ScoreServer.start_link(name: __MODULE__)
      state = ScoreServer.state(__MODULE__)

      assert state.last_accessed_at == nil
      assert state.max_number == 50
    end
  end

  describe "users_with_points_above_max/1" do
    test "it returns the users from impl and updates timestamp" do
      score = 50

      users = [
        %TopScorers.User{points: score + 5}
      ]

      timestamp = TopScorers.formatted_timestamp()

      TopScorers.Score.Mock
      |> expect(:new_max_number, fn -> score end)

      # We can't use `start_supervised` here, since we can't allow the mock to be called from the supervisor pid 
      # (since it's unknown), therefore initialization has to happen in the test Process.
      {:ok, server} = ScoreServer.start_link(name: __MODULE__)

      TopScorers.Score.Mock
      |> stub(:users_with_points_above, fn 50 -> users end)
      |> allow(self(), server)

      actual = ScoreServer.users_with_points_above_max(__MODULE__)
      state = ScoreServer.state(__MODULE__)

      assert users == actual
      assert state.last_accessed_at >= timestamp
    end
  end

  describe "periodic_update/1" do
    test "it calls out to randomize user points and updates the max_number" do
      TopScorers.Score.Mock
      |> expect(:new_max_number, fn -> 50 end)

      # We can't use `start_supervised` here, since we can't allow the mock to be called from the supervisor pid 
      # (since it's unknown), therefore initialization has to happen in the test Process.
      {:ok, server} = ScoreServer.start_link(name: __MODULE__)

      TopScorers.Score.Mock
      |> expect(:new_max_number, fn -> 60 end)
      |> expect(:randomize_user_points, fn -> 10 end)
      |> allow(self(), server)

      assert :ok == ScoreServer.periodic_update(__MODULE__)

      state = ScoreServer.state(__MODULE__)

      assert state.max_number == 60
    end
  end
end
