# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     TopScorers.Repo.insert!(%TopScorers.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias TopScorers.User

timestamp = TopScorers.formatted_timestamp()

users =
  %{inserted_at: {:placeholder, :timestamp}, updated_at: {:placeholder, :timestamp}}
  |> List.duplicate(1_000_000)

TopScorers.Repo.insert_all(User, users, placeholders: %{timestamp: timestamp})
